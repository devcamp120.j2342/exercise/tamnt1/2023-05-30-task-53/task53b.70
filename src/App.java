import java.util.Date;

import model.Customer;
import model.Visit;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Tam1");
        Customer customer2 = new Customer("Tam2");
        System.out.println(customer1 + " " + customer2);
        Visit visit1 = new Visit(customer1, new Date());
        visit1.setServiceExpense(200.00);
        visit1.setProductExpense(100.00);
        Visit visit2 = new Visit(customer2, new Date());
        System.out.println(visit1 + " " + visit2);
        System.out.println(visit1.getTotalExpense());
    }
}
